package fr.univ.model;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


@JsonInclude(Include.NON_NULL)
public class Identite {
    @JacksonXmlProperty(localName = "nom")
	private String nom;
    @JacksonXmlProperty(localName = "prenom")
	private String prenom;
    
    @XmlElement
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	@XmlElement
	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	@Override
	public String toString() {
		return "nom="+nom+", prenom="+prenom;
	}

}
