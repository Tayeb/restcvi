package fr.univ.model;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


@JsonInclude(Include.NON_NULL)
public class Expe {

	@JacksonXmlProperty(localName = "datedeb")
	private String datedeb;
	@JacksonXmlProperty(localName = "datefin")
	private String datefin;
	@JacksonXmlProperty(localName = "descript")
	private String descript;
	
	@XmlElement
	public String getDatedeb() {
		return datedeb;
	}

	public void setDatedeb(String datedeb) {
		this.datedeb = datedeb;
	}

	@XmlElement
	public String getDatefin() {
		return datefin;
	}

	public void setDatefin(String datefin) {
		this.datefin = datefin;
	}
	@XmlElement
	public String getDescript() {
		return descript;
	}

	public void setDescript(String descript) {
		this.descript = descript;
	}
	
	@Override
	public String toString() {
		return "datedeb="+datedeb+", datefin="+datefin+", descript="+descript;
	}

}
