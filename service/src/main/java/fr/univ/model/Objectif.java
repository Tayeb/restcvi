package fr.univ.model;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


@JsonInclude(Include.NON_NULL)
public class Objectif {

    @JacksonXmlProperty(localName = "stage")
	private String stage;
    @JacksonXmlProperty(localName = "emploi")
	private String emploi;
    @XmlElement
	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}
	@XmlElement
	public String getEmploi() {
		return emploi;
	}

	public void setEmploi(String emploi) {
		this.emploi = emploi;
	}
	
	@Override
	public String toString() {
		if(emploi == null) {
			return "stage="+stage;
		}
		return "emploi"+emploi;
	}

}
