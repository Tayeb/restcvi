package fr.univ.model;

import javax.xml.bind.annotation.XmlAttribute;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


@JsonInclude(Include.NON_NULL)
public class Lv {

	@JacksonXmlProperty(localName = "cert", isAttribute=true)
	
	private String cert;
	@JacksonXmlProperty(localName = "iso", isAttribute=true)
	
	private String iso;
	@JacksonXmlProperty(localName = "nivi", isAttribute=true)
	
	private String nivi;
	@JacksonXmlProperty(localName = "nivs", isAttribute=true)
	private String nivs;
	
	@XmlAttribute
	public String getCert() {
		return cert;
	}

	public void setCert(String cert) {
		this.cert = cert;
	}
	
	@XmlAttribute
	public String getIso() {
		return iso;
	}

	public void setIso(String iso) {
		this.iso = iso;
	}
	
	@XmlAttribute
	public String getNivi() {
		return nivi;
	}

	public void setNivi(String nivi) {
		this.nivi = nivi;
	}
	
	@XmlAttribute
	public String getNivs() {
		return nivs;
	}

	public void setNivs(String nivs) {
		this.nivs = nivs;
	}

}
