
package fr.univ.model;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;


@JacksonXmlRootElement(localName = "cvi")
@JsonInclude(Include.NON_NULL)
@XmlRootElement
@XmlType(propOrder = {"identite","objectif","prof","competences", "divers"})
public class Cvi {
	@Id
	private String id;

	@JacksonXmlElementWrapper(localName = "identite", useWrapping = false)
	private Identite identite;
	@JacksonXmlElementWrapper(localName = "objectif", useWrapping = false)
	private Objectif objectif = null;
	@JacksonXmlProperty(localName = "prof")
	@JacksonXmlElementWrapper(localName = "prof", useWrapping = false)
	private Prof prof = null;
	@JacksonXmlElementWrapper(localName = "competence", useWrapping = false)
	private Competences competences = null;
	@JacksonXmlElementWrapper(localName = "divers", useWrapping = false)
	private String[] divers = null;
	
	@XmlElement
	public Identite getIdentite() {
		return identite;
	}

	public void setIdentite(Identite identite) {
		this.identite = identite;
	}
	
	@XmlElement
	public Objectif getObjectif() {
		return objectif;
	}

	public void setObjectif(Objectif objectif) {
		this.objectif = objectif;
	}
	
	@XmlElement
	public Prof getProf() {
		return prof;
	}

	public void setProf(Prof prof) {
		this.prof = prof;
	}
	
	@XmlElement
	public Competences getCompetences() {
		return competences;
	}

	public void setCompetences(Competences competences) {
		this.competences = competences;
	}
	
	@XmlElement
	public String[] getDivers() {
		return divers;
	}

	public void setDivers(String[] divers) {
		this.divers = divers;
	}

	
	public String getId() { 
		return id;
	} 
	 
	 	
	@Override
	public String toString() {
		String diver_s ="[";
		for(String diver : divers) {
			diver_s+=diver+",\n";
		}
		diver_s+="]";
		return "cv{" + "identitie=" + identite.toString() 
		             + ", objectif="+objectif.toString()
		             +", profession="+prof.toString()
		             +", competence="+competences.toString()
		             +", divers{"+diver_s
		             + "}"
				+"}";
	}

}
