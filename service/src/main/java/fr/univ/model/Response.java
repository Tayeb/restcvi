package fr.univ.model;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;


@JacksonXmlRootElement(localName = "RESPONSE")
@JsonInclude(Include.NON_NULL)
public class Response {
	
	@JacksonXmlProperty(localName = "OID")
	@XmlElement
	private String oid;
	
	@JacksonXmlProperty(localName = "STATUS")
	@XmlElement
	private STATUS status;
	
	@JacksonXmlProperty(localName = "DESCRIPTION")
	@XmlElement
	private String description;
	
	public static enum STATUS {
		INSERTED,
		DELETED,
		UPDATED,
		ERROR,
		EMPTY
	}
	
	@XmlElement
	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}
	
	@XmlElement
	public STATUS getStatus() {
		return status;
	}

	public void setStatus(STATUS inserted) {
		this.status = inserted;
	}
	
	@XmlElement
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
		

}
