package fr.univ.model;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "cvs")
@Document
public class Cvs {
	@JacksonXmlElementWrapper(localName = "cv", useWrapping = false)
	private List<Cvi> cv;
	
	public List<Cvi> getCv() {
		return cv;
	}

	public void setCv(List<Cvi> cv) {
		this.cv = cv;
	}
	
	
	
}
