package fr.univ.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

@JsonInclude(Include.NON_NULL)
@XmlType(propOrder = {"diplome","certif","lv","info"})
public class Competences {

	@JacksonXmlProperty(localName = "diplome")
	@JacksonXmlElementWrapper(localName = "diplome", useWrapping = false)
	private Diplome[] diplome = new Diplome[5];

	@JacksonXmlProperty(localName = "certif")
	@JacksonXmlElementWrapper(localName = "certif", useWrapping = false)
	private List<Certif> certif = new ArrayList<Certif>();

	@JacksonXmlProperty(localName = "lv")
	@JacksonXmlElementWrapper(localName = "lv", useWrapping = false)
	private Lv[] lv = null;

	@JacksonXmlProperty(localName = "info")
	@JacksonXmlElementWrapper(localName = "info", useWrapping = false)
	
	private Info info;
	
	@XmlElement
	public Diplome[] getDiplome() {
		return diplome;
	}

	public void setDiplome(Diplome[] diplome) {
		this.diplome = diplome;
	}
	
	@XmlElement
	public List<Certif> getCertif() {
		return certif;
	}

	public void setCertif(ArrayList<Certif> certif) {
		this.certif = certif;
	}
	
	@XmlElement
	public Lv[] getLv() {
		return lv;
	}

	public void setLv(Lv[] lv) {
		this.lv = lv;
	}
	
	@XmlElement
	public Info getInfo() {
		return info;
	}

	public void setInfo(Info info) {
		this.info = info;
	}

}
