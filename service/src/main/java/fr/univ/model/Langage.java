package fr.univ.model;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


@JsonInclude(Include.NON_NULL)
public class Langage {
	
    @JacksonXmlProperty(localName = "nom")
	private String nom;
    @JacksonXmlProperty(localName = "niveau")
    private String niveau;
    @XmlElement
	public String getNom() {
		return nom;
	}
	@XmlElement
	public String getNiveau() {
		return niveau;
	}
	
	public void setNiveau(String niveau) {
		this.niveau = niveau;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}

	
	
	
}
