package fr.univ.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.ArrayList;


@JsonInclude(Include.NON_NULL)
public class Info {
    @JacksonXmlProperty(localName = "langage")
	@JacksonXmlElementWrapper(localName = "langage", useWrapping = false)
	private List<Langage> langage = new ArrayList<Langage>();
    
    @XmlElement
	public List<Langage> getLangage() {
		return langage;
	}

	public void setLangage(ArrayList<Langage> langage) {
		this.langage = langage;
	}

}
