package fr.univ.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


@JsonInclude(Include.NON_NULL)
public class Diplome {

	@JacksonXmlProperty(localName = "niveau", isAttribute = true)
	private String niveau;
	@JacksonXmlProperty(localName = "date")
	private String date;
	@JacksonXmlProperty(localName = "descript")
	private String descript;
	@JacksonXmlProperty(localName = "institut")
	private String institut;
	
	@XmlAttribute
	public String getNiveau() {
		return niveau;
	}

	public void setNiveau(String niveau) {
		this.niveau = niveau;
	}
	
	@XmlElement
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
	@XmlElement
	public String getDescript() {
		return descript;
	}

	public void setDescript(String descript) {
		this.descript = descript;
	}
	
	@XmlElement
	public String getInstitut() {
		return institut;
	}

	public void setInstitut(String institut) {
		this.institut = institut;
	}

}
