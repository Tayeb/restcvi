package fr.univ.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;


@JsonInclude(Include.NON_NULL)
public class Prof {
	@JacksonXmlProperty(localName = "expe")
	@JacksonXmlElementWrapper(localName = "expe", useWrapping = false)
	private List<Expe> expe = new ArrayList<Expe>();
	
	
	@XmlElement
	public List<Expe> getExpe() {
		return expe;
	}

	public void setExpe(ArrayList<Expe> expe) {
		this.expe = expe;
	}
	
	@Override
	public String toString() {
		String ret="[";
		for(Expe expe : expe) {
			ret+="experience"+expe.toString()+",\n";
		}
		ret+="]";
		return ret;
	}

}
