package fr.univ.config;

import java.io.File;
import java.net.URL;
import java.util.Objects;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.xml.sax.SAXException;

import fr.univ.model.Cvi;


@Configuration
public class ServiceConfiguration {
	
	// outils de validation xml 
	@Bean
	public SchemaFactory schemaFactory() {
		return SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
	}
	
	@Bean
	public Validator validator() throws SAXException {
		Schema schema = null;
		schema = schemaFactory().newSchema(new File(getResource("/validation/tp1.cvi.06.xsd")));
		return schema.newValidator();
	}
	
	@Bean
	public File file() {
		return new File(this.getResource("/validation/cv.xml"));
	}
	
	@Bean
	public StreamSource streamSource() {
		return new StreamSource(file());
	}
	
	
	// outils de marshalling
	@Bean
	public JAXBContext jaxbContext() throws JAXBException {
		return JAXBContext.newInstance(Cvi.class);
	}
	
	@Bean
	public Marshaller marshaller() throws JAXBException {
		Marshaller marshaller = jaxbContext().createMarshaller();
		marshaller.setProperty(marshaller.JAXB_FORMATTED_OUTPUT, true);
		return marshaller;
	}
	
	private String getResource(String xmlFileName) {
		URL file = getClass().getResource(xmlFileName);
		Objects.requireNonNull(file);
		return file.getFile();
	}
}
