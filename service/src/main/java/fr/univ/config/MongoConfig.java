package fr.univ.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

import com.mongodb.MongoClientURI;

@Configuration
@EnableMongoRepositories("fr.univ.model")
public class MongoConfig {

	
	@Bean
	public MongoDbFactory mongoDbFactory() {
		return new SimpleMongoDbFactory(new MongoClientURI("mongodb://ziani:kakuzu1993zt@ds241869.mlab.com:41869/cvdb"));
	}
	
	
	@Bean
	public MongoOperations mongoTemplate() {
		return new MongoTemplate(mongoDbFactory());
	}
}
