package fr.univ.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Validator;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.xml.sax.SAXException;

import fr.univ.model.Cvi;
import fr.univ.model.Cvs;
import fr.univ.model.Response;
import fr.univ.model.Response.STATUS;

@Controller
public class CviController {

	private ApplicationContext context;
	private MongoOperations mongo;
	private Validator validator;
	private Marshaller marshaller;

	public CviController() {
		this.context = new AnnotationConfigApplicationContext(fr.univ.config.MongoConfig.class,
				fr.univ.config.ServiceConfiguration.class);
		this.mongo = context.getBean(MongoOperations.class);
		this.validator = context.getBean(Validator.class);
		this.marshaller = context.getBean(Marshaller.class);
	}

	@RequestMapping(value = "/restcvi", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
	public ModelAndView homeAction() {
		return new ModelAndView("info");
	}

	@RequestMapping(value = "/restcvi/help", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
	public ModelAndView helpAction() {
		return new ModelAndView("help");
	}

	@RequestMapping(value = "/restcvi/resume", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
	public @ResponseBody Object resumeAction() throws IOException {
		Response response = new Response();
		Cvs cvs = new Cvs();
		Query query = new Query();
		query.fields().include("id");
		query.fields().include("identite");
		query.fields().include("objectif");
		List<Cvi> list = this.mongo.find(query, Cvi.class);
		if (!list.isEmpty()) {
			cvs.setCv(list);
			return cvs;
		} else {
			response.setOid(null);
			response.setStatus(STATUS.EMPTY);
			return response;
		}
	}

	@RequestMapping(value = "/restcvi/resume/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
	public @ResponseBody Object resumeAction(@PathVariable("id") String id) throws IOException {
		Response response = new Response();
		Cvi cv = this.mongo.findById(id, Cvi.class);
		if (cv != null) {
			return cv;
		} else {
			response.setOid(id);
			response.setStatus(STATUS.ERROR);
			return response;
		}
	}

	@RequestMapping(value = "/restcvi/resume/allid", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
	public @ResponseBody Object allAction() throws IOException {
		Response response = new Response();
		Query query = new Query();
		query.fields().include("_id");
		List<Cvi> cvi = this.mongo.find(query, Cvi.class);
		if (!cvi.isEmpty()) {
    		List<String> ids = new ArrayList<String>();
	    	for (Cvi cv : cvi) {
	    		ids.add(cv.getId());
	    	}
    		return ids;
		} else {
			
			response.setOid(null);
			response.setStatus(STATUS.EMPTY);
			return response;
		}
	}

	@RequestMapping(value = "/restcvi/insert", method = RequestMethod.POST, produces = MediaType.APPLICATION_XML_VALUE, consumes = MediaType.APPLICATION_XML_VALUE)
	public @ResponseBody Response insertAction(@RequestBody Cvi cvi)
			throws IOException, BeansException, SAXException, JAXBException {
		Response response = new Response();
		try {
			this.marshaller = context.getBean(Marshaller.class);
			this.marshaller.marshal(cvi, this.context.getBean(File.class));
			this.validator.validate(this.context.getBean(StreamSource.class));
			this.mongo.save(cvi);
			response.setOid(cvi.getId());
			response.setStatus(Response.STATUS.INSERTED);
			response.setDescription(null);
			return response;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			response.setOid(null);
			response.setStatus(STATUS.ERROR);
			response.setDescription(e.getMessage());
			return response;
		}
	}

	@RequestMapping(value = "/restcvi/delete/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_XML_VALUE)
	public @ResponseBody Response deleteAction(@PathVariable("id") String id) throws IOException {
		Response response = new Response();
		Cvi cv = this.mongo.findById(id, Cvi.class);
		if (cv != null) {
			Query query = new Query();
			query.addCriteria(Criteria.where("_id").is(id));
			this.mongo.remove(query, Cvi.class);
			response.setOid(id);
			response.setStatus(STATUS.DELETED);
			return response;
		} else {
			response.setOid(null);
			response.setStatus(STATUS.ERROR);
			return response;
		}
	}

	@RequestMapping(value = "/restcvi/update/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_XML_VALUE, consumes = MediaType.APPLICATION_XML_VALUE)
	public @ResponseBody Response updateAction(@PathVariable("id") String id, @RequestBody Cvi cv) throws IOException {
		Response response = new Response();
		try {
			this.marshaller = context.getBean(Marshaller.class);
			this.marshaller.marshal(cv, this.context.getBean(File.class));
			this.validator.validate(this.context.getBean(StreamSource.class));
			Query query = new Query();
			query.addCriteria(Criteria.where("_id").is(id));
			Update update = new Update();
			update.set("identite", cv.getIdentite());
			update.set("objectif", cv.getObjectif());
			update.set("prof", cv.getProf());
			update.set("competences", cv.getObjectif());
			update.set("divers", cv.getDivers());
			this.mongo.upsert(query, update, Cvi.class);
			response.setOid(id);
			response.setStatus(STATUS.UPDATED);
			response.setDescription(null);
			return response;
		} catch (Exception e) {
			response.setOid(null);
			response.setStatus(STATUS.ERROR);
			response.setDescription(e.getMessage());
			return response;
		}
	}

}
